<?php
include_once './Negocio/Libros.php';
include './Entidades/Usuario.php';

use Entidades as ent;
use Negocio as neg;

session_start();
if (!isset($_SESSION["user"])) {

    header("Location: ./login.php");
    exit();
} else {
    $usuario = $_SESSION["user"];
}
?>
<!doctype html>
<html lang="es-ES">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
        <title>Mantenedor</title>
    </head>
    <body class="container-fluid">
        <nav class="navbar navbar-dark bg-dark">
            <div class="container">
                <label class="navbar-brand">Bienvenido Usuario: <?php echo $usuario->getNombre(); ?></label>                
            </div>
        </nav>
        <div class="container">
            <h1 class="display-4 text-center mt-5 mb-5">ACTUALIZAR UN LIBRO</h1>    
            <form action="actualizarLibro.php" method="POST">
                <div class="form-group row">
                    <label class="col-4">ID</label>
                    <select class="col-8 form-control" name="id">

                        <?php
                        $libro = new neg\Libros();
                        $libros = $libro->obtenerListaLibros();
                        foreach ($libros as $lib) {
                            echo '<option>' . $lib->getId();
                        }
                        ?>

                    </select>                    
                </div>
                <div class="form-group row">
                    <label class="col-4">Nombre Libro</label>
                    <input class="col-8 form-control" type="text" name="nombrelibro" required="">
                </div>
                <div class="form-group row">
                    <label class="col-4">Autor</label>
                    <input class="col-8 form-control" type="text" name="autor" required="">
                </div>
                <div class="form-group row">
                    <label class="col-4">Editorial</label>
                    <input class="col-8 form-control" type="text" name="editorial" required="">
                </div>
                <div class="form-group row">
                    <label class="col-4">Año</label>
                    <input class="col-8 form-control" type="number" name="anio" required="">
                </div>
                <div class="form-group row">
                    <label class="col-4">Estado</label>
                    <input class="col-8 form-control" type="text" name="estado" required="">
                </div>
                <button type="submit" class="btn btn-primary">ACTUALIZAR</button>
                <a class="btn btn-secondary" href="mantenedor.php">Volver</a>
            </form>
        </div>
        <?php
        include 'php/footer.php';
        ?>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
