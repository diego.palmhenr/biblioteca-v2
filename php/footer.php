<footer class="container-fluid mt-5 bg-secondary">
    <div class="py-3 text-center">
        <h2 class="display-5">Carrera Analista Programador Computacional</h2>
        <p>Integrantes: Sebastián Labrín Urrutia - Diego Palma Henríquez - Álvaro Gutiérrez Burdiles - Ricardo Aguirre - Kevin Araneda - Francisco Cabrera</p>
        <p class="display-4">DUOC UC</p>
    </div>
</footer>

