<?php

namespace Entidades;

class Libros {

    Private $id;
    private $nombrelibro;
    private $autor;
    private $editorial;
    private $anio;
    private $estado;

    function __construct($id, $nombrelibro, $autor, $editorial, $anio, $estado) {
        $this->id = $id;
        $this->nombrelibro = $nombrelibro;
        $this->autor = $autor;
        $this->editorial = $editorial;
        $this->anio = $anio;
        $this->estado = $estado;

    }

    public function getId() {
        return $this->id;
    }

    public function getNombreLibro() {
        return $this->nombrelibro;
    }

    public function getAutor() {
        return $this->autor;
    }

    public function getEditorial() {
        return $this->editorial;
    }

    public function getAnio() {
        return $this->anio;
    }

    public function getEstado() {
        return $this->estado;
    }
}

?>